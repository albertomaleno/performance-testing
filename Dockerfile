FROM openjdk:11.0.10-oraclelinux7

VOLUME /tmp

ADD build/libs/*.jar app.jar

EXPOSE 7899

ENTRYPOINT [ "sh", "-c", "java -jar -Dserver.port=7899 /app.jar"]
