package com.gitlab.demo.hotel.stats.api.service;

import com.gitlab.demo.hotel.stats.api.AbstractSpringTestConfiguration;
import com.gitlab.demo.hotel.stats.api.cache.HotelCache;
import com.gitlab.demo.hotel.stats.api.client.ExternalAPIClient;
import com.gitlab.demo.hotel.stats.api.exceptions.ExternalAPIClientException;
import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class HotelCacheServiceWorkerTest extends AbstractSpringTestConfiguration {

    @Mock
    private ExternalAPIClient mockedExternalAPIClient;
    @Mock
    private HotelCache mockedHotelCache;
    @InjectMocks
    private HotelsCacheServiceWorker hotelsCacheServiceWorker;


    @Test
    public void can_run_on_another_thread() throws InterruptedException {
        final List<Hotel> expectedList = new ArrayList<>();
        // no await if hotels are returned at first attempt. Only enough to get the thread back
        final int totalTimeAwait = 1;
        expectedList.add(new Hotel());
        hotelsCacheServiceWorker.setCountry(Country.ES);

        when(mockedExternalAPIClient.getForHotels(Country.ES)).thenReturn(expectedList);
        when(mockedHotelCache.put(Country.ES, expectedList)).thenReturn(expectedList);
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.execute(hotelsCacheServiceWorker);
        scheduledExecutorService.awaitTermination(totalTimeAwait, TimeUnit.SECONDS);

        verify(mockedExternalAPIClient, times(1)).getForHotels(Country.ES);
        verify(mockedHotelCache, times(1)).put(Country.ES, expectedList);
    }

    @Test
    public void given_external_response_error_worker_retries() throws InterruptedException {
        final List<Hotel> expectedList = new ArrayList<>();
        // await of 4 seconds if external api fails returning them for the first time
        final int totalTimeAwait = 5;
        expectedList.add(new Hotel());
        hotelsCacheServiceWorker.setCountry(Country.ES);

        when(mockedExternalAPIClient.getForHotels(Country.ES)).thenThrow(ExternalAPIClientException.class).thenReturn(expectedList);
        when(mockedHotelCache.put(Country.ES, expectedList)).thenReturn(expectedList);
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.execute(hotelsCacheServiceWorker);
        scheduledExecutorService.awaitTermination(totalTimeAwait, TimeUnit.SECONDS);

        verify(mockedExternalAPIClient, times(2)).getForHotels(Country.ES);
        verify(mockedHotelCache, times(1)).put(Country.ES, expectedList);
    }
}
