package com.gitlab.demo.hotel.stats.api.controllers;

import com.gitlab.demo.hotel.stats.api.AbstractSpringTestConfiguration;
import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import com.gitlab.demo.hotel.stats.api.model.Report;
import com.gitlab.demo.hotel.stats.api.service.HotelsService;
import com.gitlab.demo.hotel.stats.api.service.StatsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HotelControllerTest extends AbstractSpringTestConfiguration {

    @Mock
    private HotelsService mockedHotelsService;
    @Mock
    private StatsService mockedStatsService;
    @InjectMocks
    HotelController hotelController;
    @Autowired
    private MockMvc mockMvc;
    private List<Hotel> inputList;
    private Hotel holidaysDream, incredibleHolidays, nightDream, hereAndThere;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(hotelController).build();
        inputList = new ArrayList<>();
        holidaysDream = new Hotel();
        holidaysDream.setScore(10);
        holidaysDream.setName("Holidays dream");
        holidaysDream.setId("122aa");
        holidaysDream.setCountry(Country.IT);
        incredibleHolidays = new Hotel();
        incredibleHolidays.setScore(14);
        incredibleHolidays.setName("Incredible holidays");
        incredibleHolidays.setId("3333");
        incredibleHolidays.setCountry(Country.IT);
        nightDream = new Hotel();
        nightDream.setScore(13);
        nightDream.setName("Night dream");
        nightDream.setId("4444");
        nightDream.setCountry(Country.IT);
        hereAndThere = new Hotel();
        hereAndThere.setScore(12);
        hereAndThere.setName("Here and there hotel");
        hereAndThere.setId("2222");
        hereAndThere.setCountry(Country.IT);
        inputList.add(incredibleHolidays);
        inputList.add(nightDream);
        inputList.add(hereAndThere);
        inputList.add(holidaysDream);
    }

    @Test
    public void can_get_report_from_country() throws Exception {
        final List<Hotel> top3List = new ArrayList<>(inputList.subList(0, 3));
        final double average = 12.25;
        final Report report = new Report(Country.IT, top3List, average);
        when(mockedHotelsService.getHotelsByCountry(Country.IT)).thenReturn(inputList);
        when(mockedStatsService.getAverageScore(inputList)).thenReturn(average);
        when(mockedStatsService.getTopHotels(inputList)).thenReturn(top3List);
        when(mockedStatsService.generateReport(top3List, average)).thenReturn(report);

        mockMvc.perform(get("/api/hotels/stats/report/IT"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.listHotels.size()").value(3))
                .andExpect(jsonPath("$.country").value(incredibleHolidays.getCountry().name()))
                .andExpect(jsonPath("$.listHotels[0].id").value(incredibleHolidays.getId()))
                .andExpect(jsonPath("$.listHotels[0].name").value(incredibleHolidays.getName()))
                .andExpect(jsonPath("$.listHotels[0].score").value(incredibleHolidays.getScore()))
                .andExpect(jsonPath("$.listHotels[0].country").value(incredibleHolidays.getCountry().name()))
                .andExpect(jsonPath("$.listHotels[1].id").value(nightDream.getId()))
                .andExpect(jsonPath("$.listHotels[1].name").value(nightDream.getName()))
                .andExpect(jsonPath("$.listHotels[1].score").value(nightDream.getScore()))
                .andExpect(jsonPath("$.listHotels[1].country").value(nightDream.getCountry().name()))
                .andExpect(jsonPath("$.listHotels[2].id").value(hereAndThere.getId()))
                .andExpect(jsonPath("$.listHotels[2].name").value(hereAndThere.getName()))
                .andExpect(jsonPath("$.listHotels[2].score").value(hereAndThere.getScore()))
                .andExpect(jsonPath("$.listHotels[2].country").value(hereAndThere.getCountry().name()))
                .andExpect(jsonPath("$.average").value(average))
                .andDo(print());
    }


}
