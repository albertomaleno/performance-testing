package com.gitlab.demo.hotel.stats.api.configuration;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class APIConfigurationTest {

    private APIConfiguration apiConfiguration;

    @Before
    public void setUp() {
        apiConfiguration = new APIConfiguration();
    }

    @Test
    public void can_get_host() {
        assertEquals(null, apiConfiguration.getAPIHost());
    }

    @Test
    public void can_get_protocol() {
        assertEquals(null, apiConfiguration.getAPIProtocol());
    }

    @Test
    public void can_get_resource() {
        assertEquals(null, apiConfiguration.getAPIHotelsResource());
    }

}
