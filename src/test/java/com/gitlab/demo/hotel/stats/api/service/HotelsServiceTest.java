package com.gitlab.demo.hotel.stats.api.service;

import com.gitlab.demo.hotel.stats.api.AbstractSpringTestConfiguration;
import com.gitlab.demo.hotel.stats.api.cache.HotelCache;
import com.gitlab.demo.hotel.stats.api.client.ExternalAPIClient;
import com.gitlab.demo.hotel.stats.api.exceptions.ExternalAPIClientException;
import com.gitlab.demo.hotel.stats.api.exceptions.ServiceException;
import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class HotelsServiceTest extends AbstractSpringTestConfiguration {


    @Mock
    private ExternalAPIClient mockedExternalAPIClient;
    @Mock
    private HotelCache mockedHotelCache;
    @InjectMocks
    private HotelsService hotelsService;


    @Test
    public void can_get_hotel_list_by_country_from_external_api() {
        final List<Hotel> expectedList = new ArrayList<>();
        Hotel hotelA = new Hotel();
        hotelA.setId("1");
        Hotel hotelB = new Hotel();
        hotelB.setId("2");
        expectedList.add(hotelA);
        expectedList.add(hotelB);

        when(mockedHotelCache.get(Country.ES)).thenReturn(null);
        when(mockedExternalAPIClient.getForHotels(Country.ES)).thenReturn(expectedList);
        List<Hotel> actual = hotelsService.getHotelsByCountry(Country.ES);

        verify(mockedHotelCache, times(1)).put(Country.ES, expectedList);
        assertEquals(expectedList, actual);
    }

    @Test(expected = ServiceException.class)
    public void given_error_response_from_external_api_service_exception_is_thrown() {
        final List<Hotel> expectedList = new ArrayList<>();
        Hotel hotelA = new Hotel();
        hotelA.setId("1");
        Hotel hotelB = new Hotel();
        hotelB.setId("2");
        expectedList.add(hotelA);
        expectedList.add(hotelB);

        when(mockedHotelCache.get(Country.ES)).thenReturn(null);
        Mockito.doThrow(ExternalAPIClientException.class).when(mockedExternalAPIClient).getForHotels(Country.ES);
        hotelsService.getHotelsByCountry(Country.ES);
    }

    @Test
    public void given_cached_country_returns_hotels_from_cache() {
        final List<Hotel> expectedList = new ArrayList<>();
        Hotel hotelA = new Hotel();
        hotelA.setId("1");
        Hotel hotelB = new Hotel();
        hotelB.setId("2");
        expectedList.add(hotelA);
        expectedList.add(hotelB);

        when(mockedHotelCache.get(Country.ES)).thenReturn(expectedList);
        List<Hotel> actual = hotelsService.getHotelsByCountry(Country.ES);

        assertEquals(expectedList, actual);
    }
}
