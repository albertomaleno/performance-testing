package com.gitlab.demo.hotel.stats.api.service;

import com.gitlab.demo.hotel.stats.api.AbstractSpringTestConfiguration;
import com.gitlab.demo.hotel.stats.api.cache.StatsCache;
import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import com.gitlab.demo.hotel.stats.api.model.Report;
import com.gitlab.demo.hotel.stats.api.strategy.FilterStrategy;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class StatsServiceTest extends AbstractSpringTestConfiguration {

    private static final List<Hotel> LIST_HOTELS = new ArrayList<>();
    private static final Country COUNTRY = Country.ES;
    @Mock
    private StatsCache mockedStatsCache;
    @Mock
    private FilterStrategy filterStrategy;
    @InjectMocks
    private StatsService statsService;

    @BeforeClass
    public static void setUpClass(){
        final Hotel a = new Hotel();
        a.setScore(3);
        a.setCountry(COUNTRY);
        final Hotel c = new Hotel();
        c.setScore(3);
        c.setCountry(COUNTRY);
        final Hotel b = new Hotel();
        b.setScore(4);
        b.setCountry(COUNTRY);
        final Hotel d = new Hotel();
        d.setScore(5);
        d.setCountry(COUNTRY);
        LIST_HOTELS.add(a);
        LIST_HOTELS.add(b);
        LIST_HOTELS.add(c);
        LIST_HOTELS.add(d);
    }

    @Test
    public void given_4_hotels_computes_average() {
        final double expectedAverageScore = 3.7;

        double actual = statsService.getAverageScore(LIST_HOTELS);

        assertEquals(expectedAverageScore, actual, 0);
    }

    @Test
    public void given_average_already_computed_returns_average_from_cache() {
        final double expectedAverageScore = 3.7;
        final Country country = COUNTRY;
        when(mockedStatsCache.containsKey(country)).thenReturn(true);
        when(mockedStatsCache.get(country)).thenReturn(new Report(country, LIST_HOTELS, expectedAverageScore));

        statsService.getAverageScore(LIST_HOTELS);

        verify(mockedStatsCache, times(1)).get(country);
    }

    @Test
    public void given_4_hotels_returns_top_3() {
        when(filterStrategy.filterHotels(LIST_HOTELS)).thenReturn(LIST_HOTELS);
        when(filterStrategy.removeDuplicates(LIST_HOTELS)).thenReturn(LIST_HOTELS);

        List<Hotel> actual = statsService.getTopHotels(LIST_HOTELS);

        assertEquals(LIST_HOTELS.subList(0, 3), actual);
    }

    @Test
    public void given_top_already_computed_returns_top_3_from_cache() {
        when(mockedStatsCache.containsKey(COUNTRY)).thenReturn(true);
        when(mockedStatsCache.get(COUNTRY)).thenReturn(new Report(COUNTRY, LIST_HOTELS, 0));

        statsService.getTopHotels(LIST_HOTELS);

        verify(mockedStatsCache, times(1)).get(COUNTRY);
    }

    @Test
    public void given_top_hotels_and_total_average_returns_report() {
        final double averageScore = 3.7;
        final Report expectedReport = new Report(COUNTRY, LIST_HOTELS, averageScore);
        when(mockedStatsCache.put(COUNTRY, expectedReport)).thenReturn(expectedReport);

        Report actual = statsService.generateReport(LIST_HOTELS, averageScore);

        assertEquals(expectedReport, actual);
    }

}
