package com.gitlab.demo.hotel.stats.api.cache;

import com.gitlab.demo.hotel.stats.api.AbstractSpringTestConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class StatsCacheTest extends AbstractSpringTestConfiguration {

    @Autowired
    private StatsCache statsCacheFirstReference;
    @Autowired
    private StatsCache statsCacheSecondReference;

    @Test
    public void can_get_single_instance() {
        Assert.assertEquals(statsCacheFirstReference, statsCacheSecondReference);
    }
}
