package com.gitlab.demo.hotel.stats.api.exceptions;

import org.junit.Test;

public class ServiceExceptionTest {


    @Test(expected = ServiceException.class)
    public void can_throw_error() {
        throw new ServiceException(Errors.ERROR_RETRIEVING_HOTELS);
    }

    @Test(expected = ServiceException.class)
    public void can_throw_error_and_throwable() {
        throw new ServiceException(Errors.ERROR_RETRIEVING_HOTELS, new NullPointerException());
    }
}
