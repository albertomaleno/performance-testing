package com.gitlab.demo.hotel.stats.api.benchmarking;

import com.gitlab.demo.hotel.stats.api.cache.StatsCache;
import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import com.gitlab.demo.hotel.stats.api.model.Report;
import com.gitlab.demo.hotel.stats.api.service.StatsService;
import com.gitlab.demo.hotel.stats.api.strategy.FilterByScoreAndNameStrategy;
import com.gitlab.demo.hotel.stats.api.strategy.FilterByScoreStrategy;
import com.gitlab.demo.hotel.stats.api.strategy.FilterByScoreWithPossibleDuplicationStrategy;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Comparison for different filtering strategies. This class contains 3 different tests for each implementation.
 *
 * <p>The purpose of this test is to create a microbenchmark in order to learn about them. In order to check the
 * three difference strategies, cache has been removed.</p>
 *
 * <p>As expected, the overhead of comparing score and names is greater than the one only comparing scores and allowing
 * duplicates. The implementation for a filtering strategy which does not allow to remove duplicates from the list
 * is the fastest one because it removes the overhead of O(n) where n is the number of elements to remove from the list</p>
 */
public class FilterBenchmarkTest {

    private final int iterations = 500;
    private final int inputSize = 1000;
    private StatsService statsService = new StatsService(new DummyCache(), new FilterByScoreAndNameStrategy());
    private List<Hotel> input;

    // Variable is volatile because this way is compulsory for the jvm to read it after assignment
    // if it wasn't volatile, reading would be skipped and the test wouldn't be a real world test
    private volatile List<Hotel> outputList;

    /**
     * Builds a list of random hotels with random names and scores.
     * @return randomListOfHotels
     */
    private List<Hotel> getRandomHotelList(){
        final List<Hotel> hotelList = new ArrayList<>();
        final Random random = new Random();

        for (int i = 0; i < inputSize; i++){
            Hotel hotel = new Hotel();
            hotel.setCountry(Country.FR);
            String randomString = random.ints(1, 100 + 1)
                    .limit(10)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();
            hotel.setId(randomString);
            hotel.setName(randomString);
            double scoreFrom1To10 = (1 + (10 - 1) * random.nextDouble());
            int twoDecimalsScore = (int)(scoreFrom1To10 * 100);
            scoreFrom1To10 = (double)twoDecimalsScore / 100;
            hotel.setScore(scoreFrom1To10);
            hotelList.add(hotel);
        }
        return hotelList;
    }

    @Test
    // 22 ms
    public void top_hotels_filtering_by_score_and_name_computation_time(){
        input = getRandomHotelList();

        testTopHotels(true, input);
        testTopHotels(false, input);
    }

    private void testTopHotels(boolean warmup, List<Hotel> input){
        long then = System.currentTimeMillis();
        for (int i = 0; i < iterations; i++){
            outputList = statsService.getTopHotels(input);
        }
        if (!warmup){
            long now = System.currentTimeMillis();
            System.out.println("Total time " + (now - then) + " ms");
        }
    }

    @Test
    // 10 ms
    public void top_hotels_filtering_only_by_score_and_allowing_duplicates_computation_time(){
        statsService = new StatsService(new DummyCache(), new FilterByScoreWithPossibleDuplicationStrategy());
        input = getRandomHotelList();

        testTopHotels(true, input);
        testTopHotels(false, input);
    }


    @Test
    // 19 ms
    public void top_hotels_filtering_only_by_score_with_duplicates_removal_computation_time(){
        statsService = new StatsService(new DummyCache(), new FilterByScoreStrategy());
        input = getRandomHotelList();

        testTopHotels(true, input);
        testTopHotels(false, input);
    }


    /**
     * Here the cache is extended to skip reports caching
     */
    class DummyCache extends StatsCache {
        @Override
        public Report put(Country key, Report value) {
            return null;
        }

        @Override
        public Report get(Object key) {
            return null;
        }
    }


}
