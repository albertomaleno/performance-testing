package com.gitlab.demo.hotel.stats.api.exceptions;

import org.junit.Test;

public class ExternalAPI {

    private ExternalAPIClientException externalAPIClientException;

    @Test(expected = ExternalAPIClientException.class)
    public void can_throw_error() {
        throw new ExternalAPIClientException(Errors.ERROR_RETRIEVING_HOTELS);
    }

    @Test(expected = ExternalAPIClientException.class)
    public void can_throw_error_and_throwable() {
        throw new ExternalAPIClientException(Errors.ERROR_RETRIEVING_HOTELS, new NullPointerException());
    }
}
