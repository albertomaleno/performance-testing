package com.gitlab.demo.hotel.stats.api;

import org.springframework.test.context.TestPropertySource;


/**
 * Configuration shared by all the Spring test cases.
 */
@TestPropertySource(locations = "classpath:application-test.properties")
public abstract class AbstractSpringTestConfiguration {


}
