package com.gitlab.demo.hotel.stats.api.client;

import com.gitlab.demo.hotel.stats.api.AbstractSpringTestConfiguration;
import com.gitlab.demo.hotel.stats.api.configuration.APIConfiguration;
import com.gitlab.demo.hotel.stats.api.exceptions.ExternalAPIClientException;
import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ExternalAPIClientTest extends AbstractSpringTestConfiguration {

    private static final String API_HOST = "localhost";
    private static final String API_PROTOCOL = "http";
    private static final String API_HOTELS_RESOURCE = "/hotels/{iso}";
    private static final String API_URL = API_PROTOCOL + "://" + API_HOST + API_HOTELS_RESOURCE.replace("{iso}", Country.ES.name().toLowerCase());

    @Mock
    private APIConfiguration apiConfiguration;
    @Mock
    private RestTemplate mockedRestTemplate;
    @InjectMocks
    private ExternalAPIClient externalAPIClient;

    @Before
    public void setUp() {
        when(apiConfiguration.getAPIHost()).thenReturn(API_HOST);
        when(apiConfiguration.getAPIProtocol()).thenReturn(API_PROTOCOL);
        when(apiConfiguration.getAPIHotelsResource()).thenReturn(API_HOTELS_RESOURCE);
    }


    @Test
    public void given_a_country_returns_list_hotels() {
        final List<Hotel> expectedList = new ArrayList<>();
        Hotel hotelA = new Hotel();
        hotelA.setId("1");
        Hotel hotelB = new Hotel();
        hotelB.setId("2");
        expectedList.add(hotelA);
        expectedList.add(hotelB);
        final ResponseEntity<List<Hotel>> expectedResponseEntity = new ResponseEntity<>(expectedList, HttpStatus.OK);
        when(mockedRestTemplate.exchange(API_URL, HttpMethod.GET, null, new ParameterizedTypeReference<List<Hotel>>() {
        })).thenReturn(expectedResponseEntity);

        List<Hotel> actual = externalAPIClient.getForHotels(Country.ES);

        Assert.assertEquals(expectedList, actual);
    }

    @Test(expected = ExternalAPIClientException.class)
    public void if_external_api_returns_error_client_throws_exception() {
        doThrow(RestClientException.class).when(mockedRestTemplate)
                .exchange(API_URL, HttpMethod.GET, null, new ParameterizedTypeReference<List<Hotel>>() {
                });

        externalAPIClient.getForHotels(Country.ES);
    }
}
