package com.gitlab.demo.hotel.stats.api.converters;

import com.gitlab.demo.hotel.stats.api.model.Country;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class StringToCountryConverterTest {

    @Autowired
    private StringToCountryConverter stringToCountryConverter;

    @Test
    public void given_lowercase_country_string_returns_country_enum() {
        final String givenStr = "es";
        final Country expectedCountry = Country.ES;

        Country actual = stringToCountryConverter.convert(givenStr);

        assertEquals(expectedCountry, actual);
    }

    @Test
    public void given_uppercase_country_string_returns_country_enum() {
        final String givenStr = "ES";
        final Country expectedCountry = Country.ES;

        Country actual = stringToCountryConverter.convert(givenStr);

        assertEquals(expectedCountry, actual);
    }

    @Test
    public void given_non_existent_country_string_returns_null() {
        final String givenStr = "QQ";
        final Country expectedCountry = null;

        Country actual = stringToCountryConverter.convert(givenStr);

        assertEquals(expectedCountry, actual);
    }
}
