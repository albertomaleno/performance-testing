package com.gitlab.demo.hotel.stats.api.strategy;

import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FilterByScoreAndNameStrategyTest {

    private FilterStrategy filterByScoreAndNameStrategy;
    private List<Hotel> unorderedList;
    private Hotel firstHotel, secondHotel, thirdHotel, fourthHotel;


    @Before
    public void setUp() {
        unorderedList = new ArrayList<>();
        firstHotel = new Hotel();
        firstHotel.setName("b");
        firstHotel.setScore(4);
        firstHotel.setCountry(Country.ES);
        secondHotel = new Hotel();
        secondHotel.setName("d");
        secondHotel.setScore(3);
        secondHotel.setCountry(Country.ES);
        thirdHotel = new Hotel();
        thirdHotel.setName("a");
        thirdHotel.setScore(3);
        thirdHotel.setCountry(Country.ES);
        fourthHotel = new Hotel();
        fourthHotel.setName("c");
        fourthHotel.setScore(4);
        fourthHotel.setCountry(Country.ES);
        unorderedList.add(thirdHotel);
        unorderedList.add(secondHotel);
        unorderedList.add(fourthHotel);
        unorderedList.add(firstHotel);

        filterByScoreAndNameStrategy = new FilterByScoreAndNameStrategy();
    }

    @Test
    public void given_a_list_of_hotels_sorts_by_score_first_and_then_by_name() {
        final List<Hotel> expectedList = new ArrayList<>();
        expectedList.add(firstHotel);
        expectedList.add(fourthHotel);
        expectedList.add(thirdHotel);
        expectedList.add(secondHotel);

        List<Hotel> actual = filterByScoreAndNameStrategy.filterHotels(unorderedList);

        assertEquals(expectedList, actual);
    }

    @Test
    public void given_a_list_of_hotels_removes_duplicates() {
        final List<Hotel> expectedList = new ArrayList<>();
        expectedList.add(thirdHotel);
        expectedList.add(fourthHotel);

        List<Hotel> actual = filterByScoreAndNameStrategy.removeDuplicates(unorderedList);

        assertEquals(expectedList, actual);
    }
}
