package com.gitlab.demo.hotel.stats.api.client;

import com.gitlab.demo.hotel.stats.api.configuration.APIConfiguration;
import com.gitlab.demo.hotel.stats.api.exceptions.Errors;
import com.gitlab.demo.hotel.stats.api.exceptions.ExternalAPIClientException;
import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class ExternalAPIClient {

    private final APIConfiguration apiConfiguration;
    private final RestTemplate restTemplate;

    @Autowired
    public ExternalAPIClient(APIConfiguration apiConfiguration, RestTemplate restTemplate) {
        this.apiConfiguration = apiConfiguration;
        this.restTemplate = restTemplate;
    }

    public List<Hotel> getForHotels(Country country) {
        List<Hotel> listHotels;
        final String url = apiConfiguration.getAPIProtocol() + "://" + apiConfiguration.getAPIHost()
                + apiConfiguration.getAPIHotelsResource().replace("{iso}", country.name().toLowerCase());

        ParameterizedTypeReference<List<Hotel>> parameterType = new ParameterizedTypeReference<>() {
        };
        try {
            listHotels = restTemplate
                    .exchange(url, HttpMethod.GET, null, parameterType)
                    .getBody();
        } catch (RestClientException e) {
            throw new ExternalAPIClientException(Errors.ERROR_RETRIEVING_HOTELS, e);
        }
        return listHotels;
    }
}
