package com.gitlab.demo.hotel.stats.api.strategy;

import com.gitlab.demo.hotel.stats.api.model.Hotel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * An implementation which filters only by score, making possible to have duplicates.
 *
 */
public class FilterByScoreWithPossibleDuplicationStrategy implements FilterStrategy {

    private List<Hotel> filteredList;


    @Override
    public List<Hotel> filterHotels(List<Hotel> listHotels) {
        Comparator<Hotel> scoreComparator = Comparator.comparing(Hotel::getScore);
        Collections.sort(listHotels, scoreComparator);
        filteredList = listHotels;
        return filteredList;
    }

    @Override
    public List<Hotel> removeDuplicates(List<Hotel> listHotels) {
        return filteredList;
    }
}
