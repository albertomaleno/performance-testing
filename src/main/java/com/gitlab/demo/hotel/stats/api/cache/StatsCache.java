package com.gitlab.demo.hotel.stats.api.cache;


import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Report;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;

/**
 * Cache for the reports generated from the top hotels of a country. It holds a report for each country available.
 *
 * <p>The scope of this class is singleton so only one cache will be maintained across all the services.
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class StatsCache extends LinkedHashMap<Country, Report> {

    /**
     * This field sets the capacity for the LinkedHashMap. It is set to 3 because there will be only 3 countries
     * with report.
     */
    private static final int INITIAL_CAPACITY = Country.values().length;


    public StatsCache() {
        super(INITIAL_CAPACITY, 0.75f, true);
    }

}
