package com.gitlab.demo.hotel.stats.api.exceptions;

public class ExternalAPIClientException extends RuntimeException {

    public ExternalAPIClientException(Errors errors) {
        super(errors.getErrorCode());
    }

    public ExternalAPIClientException(Errors errors, Throwable cause) {
        super(errors.getErrorCode(), cause);
    }
}
