package com.gitlab.demo.hotel.stats.api.strategy;

import com.gitlab.demo.hotel.stats.api.model.Hotel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * This interface defines an strategy to be followed by each implementation
 * in order to filter hotels.
 *
 * Each implementation class must define a way to filter hotels but also a way to remove
 * duplicates so the resulting list contains no duplications.
 */
public interface FilterStrategy {

    /**
     * Filter hotels depending on its fields. Implementation must defined which rules
     * to follow in order to filter the list of hotels.
     *
     * @param listHotels is the list to sort
     * @return a sorted list of hotels
     */
    List<Hotel> filterHotels(List<Hotel> listHotels);

    /**
     * Removes hotel duplications from the list. The implementation class must define
     * which field can not be duplicated.
     *
     * @param listHotels is the list to remove scores from
     * @return a list with no duplicates
     */
    default List<Hotel> removeDuplicates(List<Hotel> listHotels){
        List<Hotel> filteredList = new ArrayList<>();
        if (listHotels.isEmpty()) return listHotels;
        else filteredList.add(listHotels.get(0));
        for (int i = 0; i < listHotels.size() - 1; i++) {
            Hotel current = listHotels.get(i);
            Hotel next = listHotels.get(i + 1);
            if (current.getScore() != next.getScore()) {
                filteredList.add(next);
            }
        }
        return filteredList;
    }
}
