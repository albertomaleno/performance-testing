package com.gitlab.demo.hotel.stats.api.service;

import com.gitlab.demo.hotel.stats.api.cache.HotelCache;
import com.gitlab.demo.hotel.stats.api.client.ExternalAPIClient;
import com.gitlab.demo.hotel.stats.api.exceptions.Errors;
import com.gitlab.demo.hotel.stats.api.exceptions.ExternalAPIClientException;
import com.gitlab.demo.hotel.stats.api.exceptions.ServiceException;
import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


/**
 * Class responsible for retrieving the hotels from external API and mapping them
 * into Hotel models.
 *
 * <p>This class makes use of a HotelsCache as the external API may fail while returning the list of hotels
 * from a country and the cost of retrieving them each time, can be expensive.
 *
 * <p>The cache is automatically updated every 5 minutes starting at boot with a runnable task to
 * get the hotels from the external API.
 */
@Service
public class HotelsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HotelsService.class);
    private final HotelCache hotelCache;
    private final ExternalAPIClient externalAPIClient;


    public HotelsService(@Autowired HotelCache hotelCache, ExternalAPIClient externalAPIClient) {
        this.hotelCache = hotelCache;
        this.externalAPIClient = externalAPIClient;
    }

    /**
     * Gets the list of hotels present on a country. If the cache contains the list, then it is returned from it,
     * if not, a new request to the external API is done.
     *
     * @param country is the country to get the hotels from
     * @return list containing the hotels from the country
     */
    public List<Hotel> getHotelsByCountry(Country country) {
        List<Hotel> listHotels;

        if (hotelCache.get(country) != null)
            listHotels = hotelCache.get(country);
        else {
            try {
                listHotels = externalAPIClient.getForHotels(country);
                hotelCache.put(country, listHotels);
            } catch (ExternalAPIClientException e) {
                throw new ServiceException(Errors.ERROR_RETRIEVING_HOTELS);
            }
        }
        return listHotels;
    }

    @Scheduled(fixedDelayString = "${cache.delay.time}")
    private void fillCache() {
        LOGGER.debug("Executing scheduled task to fill the cache, total number of countries: {}", Country.values().length);
        Country[] countries = Country.values();
        var scheduledExecutorService = Executors.newScheduledThreadPool(countries.length);
        for (Country country : countries) {
            var hotelsCacheServiceWorker = new HotelsCacheServiceWorker(externalAPIClient, hotelCache);
            hotelsCacheServiceWorker.setCountry(country);
            scheduledExecutorService.schedule(hotelsCacheServiceWorker, 4, TimeUnit.SECONDS);
        }
        scheduledExecutorService.shutdown();
        while (!scheduledExecutorService.isTerminated()) {
        }
        LOGGER.info("All threads finished running. Cache is ready");
    }
}
