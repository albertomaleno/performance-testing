package com.gitlab.demo.hotel.stats.api.exceptions;

public class ServiceException extends RuntimeException {

    public ServiceException(Errors error) {
        super(error.getErrorCode());
    }

    public ServiceException(Errors error, Throwable cause) {
        super(error.getErrorCode(), cause);
    }

}
