package com.gitlab.demo.hotel.stats.api.converters;

import com.gitlab.demo.hotel.stats.api.annotations.RequestParameterConverter;
import com.gitlab.demo.hotel.stats.api.model.Country;
import org.springframework.core.convert.converter.Converter;


@RequestParameterConverter
public class StringToCountryConverter implements Converter<String, Country> {


    @Override
    public Country convert(String source) {
        for (Country country : Country.values()) {
            if (source.equalsIgnoreCase(country.name())) {
                return country;
            }
        }
        return null;
    }
}
