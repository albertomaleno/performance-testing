package com.gitlab.demo.hotel.stats.api.model;

import java.util.List;
import java.util.Objects;

/**
 * Representation of a report of hotel stats from a country.
 */
public class Report {

    private Country country;
    private List<Hotel> listHotels;
    private double average;

    public Report(Country country, List<Hotel> listHotels, double average) {
        this.country = country;
        this.listHotels = listHotels;
        this.average = average;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<Hotel> getListHotels() {
        return listHotels;
    }

    public void setListHotels(List<Hotel> listHotels) {
        this.listHotels = listHotels;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    @Override
    public String toString() {
        return country.name() + "(" + country.getCountry() + "): " +
                average + " average, Top " + listHotels.size() + ": "
                + listHotels.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Report report = (Report) o;
        return Double.compare(report.average, average) == 0 &&
                country == report.country &&
                Objects.equals(listHotels, report.listHotels);
    }

}
