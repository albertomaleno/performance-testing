package com.gitlab.demo.hotel.stats.api.exceptions;

public enum Errors {

    ERROR_RETRIEVING_HOTELS("External API failed returning hotels"),
    ERROR_AWAITING_FOR_EXTERNAL_RESPONSE("Failed retrying to get hotels from external API"),
    ERROR_FILLING_CACHE("Can not fill cache, external API failed returning hotels"),
    ERROR_ON_THREAD_SLEEP("Error on thread sleep while trying to retrieve hotels from external api");


    private final String errorCode;

    Errors(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
