package com.gitlab.demo.hotel.stats.api.controllers;

import com.gitlab.demo.hotel.stats.api.exceptions.ServiceException;
import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import com.gitlab.demo.hotel.stats.api.model.Report;
import com.gitlab.demo.hotel.stats.api.service.HotelsService;
import com.gitlab.demo.hotel.stats.api.service.StatsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("api/hotels/stats")
@Api(protocols = "http", produces = "application/json")
public class HotelController {

    private final HotelsService hotelsService;
    private final StatsService statsService;

    @Autowired
    public HotelController(HotelsService hotelsService, StatsService statsService) {
        this.hotelsService = hotelsService;
        this.statsService = statsService;
    }

    @ApiOperation("Returns a report based on the average score of the hotels in a country and the top 3")
    @GetMapping(value = "/report/{country}", produces = "application/json")
    @CrossOrigin
    public @ResponseBody
    Report getReport(@PathVariable Country country) {
        try {
            List<Hotel> listHotels = hotelsService.getHotelsByCountry(country);
            double average = statsService.getAverageScore(listHotels);
            List<Hotel> filteredHotels = statsService.getTopHotels(listHotels);
            return statsService.generateReport(filteredHotels, average);
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "No hotel available error");
        }
    }


}
