package com.gitlab.demo.hotel.stats.api.service;

import com.gitlab.demo.hotel.stats.api.cache.HotelCache;
import com.gitlab.demo.hotel.stats.api.client.ExternalAPIClient;
import com.gitlab.demo.hotel.stats.api.exceptions.Errors;
import com.gitlab.demo.hotel.stats.api.exceptions.ExternalAPIClientException;
import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class HotelsCacheServiceWorker implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(HotelsService.class);
    private static final Integer HOTEL_REQUEST_RETRY_TIME = 4000;
    private static final Integer TOTAL_RETRIES = 10;
    private final ExternalAPIClient externalAPIClient;
    private final HotelCache hotelCache;
    private Country country;

    public HotelsCacheServiceWorker(ExternalAPIClient externalAPIClient, HotelCache hotelCache) {
        this.externalAPIClient = externalAPIClient;
        this.hotelCache = hotelCache;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public void run() {
        List<Hotel> listHotels = new ArrayList<>();
        int retryCount = 0;
        while (listHotels.isEmpty() && retryCount++ < TOTAL_RETRIES) {
            try {
                LOGGER.debug("Retrieving hotels from country: {}", country);
                listHotels = externalAPIClient.getForHotels(country);
            } catch (ExternalAPIClientException e) {
                LOGGER.warn(Thread.currentThread().getName() + " failed retrieving hotels from external", e);
                try {
                    Thread.sleep(HOTEL_REQUEST_RETRY_TIME);
                } catch (InterruptedException ie) {
                    LOGGER.error(Errors.ERROR_ON_THREAD_SLEEP.getErrorCode(), ie);
                }
            }
        }
        hotelCache.put(country, listHotels);
    }
}
