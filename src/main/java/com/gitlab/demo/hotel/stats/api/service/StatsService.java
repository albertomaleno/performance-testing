package com.gitlab.demo.hotel.stats.api.service;

import com.gitlab.demo.hotel.stats.api.cache.StatsCache;
import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import com.gitlab.demo.hotel.stats.api.model.Report;
import com.gitlab.demo.hotel.stats.api.strategy.FilterStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Class responsible for computing the top 3 and average score from a list of hotels.
 *
 * <p>This class makes use of a stats cache to only compute again the top and average score if the data is not
 * present in the cache.
 *
 * <p>The cache is automatically updated every 2 minutes from elements from the hotels cache.
 */
@Service
public class StatsService {

    private final StatsCache statsCache;
    private final FilterStrategy filterStrategy;

    @Autowired
    public StatsService(StatsCache statsCache, FilterStrategy filterStrategy) {
        this.statsCache = statsCache;
        this.filterStrategy = filterStrategy;
    }

    /**
     * Gets the average score from the hotels in the list.
     * If average is present in the cache, the average will be returned from there instead
     * of calculating it again.
     *
     * @param listHotels is the list of hotels to get the average from
     * @return average score
     */
    public double getAverageScore(List<Hotel> listHotels) {
        if (statsCache.containsKey(listHotels.get(0).getCountry())) {
            return statsCache.get(listHotels.get(0).getCountry()).getAverage();
        }
        double average = 0;
        int size = listHotels.size();
        for (Hotel hotel : listHotels) {
            average += hotel.getScore();
        }
        int tmp = (int) ((average / size) * 10.0);
        return ((double) tmp) / 10.0;
    }

    /**
     * Gets the top 3 hotels in the list. The algorithm first sorts the values from the list by score and name
     * and then, it removes the duplicate scores to only show different hotels from each score.
     * <p>
     * If the top hotels are present in the cache, the top will be returned from there instead of computing
     * them again.
     *
     * @param listHotels is the list of hotels to get the top from
     * @return top hotels
     */
    public List<Hotel> getTopHotels(List<Hotel> listHotels) {
        if (statsCache.containsKey(listHotels.get(0).getCountry())) {
            return statsCache.get(listHotels.get(0).getCountry()).getListHotels();
        }
        List<Hotel> sortedList = filterStrategy.filterHotels(listHotels);
        List<Hotel> filteredList = filterStrategy.removeDuplicates(sortedList);
        List<Hotel> top3List = new ArrayList<>(filteredList.subList(0, 3));
        return top3List;
    }

    /**
     * Generates a report based on a list of hotels containing the top 3 and an average score retrieved from
     * all the hotels. This method assumes a new report has to be generated and so, it replaces the current, if any,
     * in the cache.
     *
     * @param listHotels   is the top 3 hotel list
     * @param averageScore is the average score from all the hotels in a country
     * @return a report with this data
     */
    public Report generateReport(List<Hotel> listHotels, double averageScore) {
        Country country = listHotels.get(0).getCountry();
        Report report = new Report(country, listHotels, averageScore);
        statsCache.put(country, report);
        return report;
    }
}
