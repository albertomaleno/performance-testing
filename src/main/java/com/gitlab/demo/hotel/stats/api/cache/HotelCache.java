package com.gitlab.demo.hotel.stats.api.cache;

import com.gitlab.demo.hotel.stats.api.model.Country;
import com.gitlab.demo.hotel.stats.api.model.Hotel;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Cache for hotels retrieved from external API. Each entry of this map is composed by a key, being the country,
 * and a value, being the list of hotels present on that country.
 *
 * <p>The scope of this class is a singleton, so no other instance of it can be created. The instance is
 * going to be shared by the hotel and stat services.
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class HotelCache extends ConcurrentHashMap<Country, List<Hotel>> {

    /**
     * This field sets the capacity for the LinkedHashMap. It is set to the length of the country total number of values
     * present on the enum as there will be a report for each country.
     */
    private static final int INITIAL_CAPACITY = Country.values().length;

    public HotelCache() {
        super(INITIAL_CAPACITY);
    }
}
