package com.gitlab.demo.hotel.stats.api.strategy;

import com.gitlab.demo.hotel.stats.api.model.Hotel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Implementation class for a filtering strategy.
 *
 * <p>It filters hotels first, by score, having the higher ones
 * the top positions in the list, and then, by name,
 * just in case a hotel has the same score as other may have.
 *
 * <p>No duplications are allowed in the resulting list, meaning
 * that hotels with slower score are shown instead of the higher ones.
 *
 * <p>This way, we give the opportunity to the hotels with lesser
 * score to be shown in the stats. There is another reason to implement
 * the filtering on this way, and its simply because having more than
 * one filter (score and name), is more a similar approach to a real world
 * algorithm.
 *
 * <p>If we take some well know algorithms like
 * the Google Maps one, we can check that, by default, they first filter by distance and
 * then, by score.
 */
@Component("filterStrategy")
public class FilterByScoreAndNameStrategy implements FilterStrategy {


    @Override
    public List<Hotel> filterHotels(List<Hotel> listHotels) {
        Comparator<Hotel> scoreComparator = Comparator.comparing(Hotel::getScore);
        Comparator<Hotel> nameComparator = Comparator.comparing(Hotel::getName);
        Comparator<Hotel> multipleComparison = scoreComparator.reversed().thenComparing(nameComparator);
        Collections.sort(listHotels, multipleComparison);
        return listHotels;
    }
}
