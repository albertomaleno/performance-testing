package com.gitlab.demo.hotel.stats.api.strategy;

import com.gitlab.demo.hotel.stats.api.model.Hotel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * An implementation which filters only by score, making possible to have duplicates.
 *
 */
public class FilterByScoreStrategy implements FilterStrategy {


    @Override
    public List<Hotel> filterHotels(List<Hotel> listHotels) {
        Comparator<Hotel> scoreComparator = Comparator.comparing(Hotel::getScore);
        Collections.sort(listHotels, scoreComparator);
        return listHotels;
    }

}
