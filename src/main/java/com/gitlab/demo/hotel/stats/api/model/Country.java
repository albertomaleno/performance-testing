package com.gitlab.demo.hotel.stats.api.model;

public enum Country {

    ES("Spain"), IT("Italy"), FR("France");

    private final String country;

    Country(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }
}
