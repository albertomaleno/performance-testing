package com.gitlab.demo.hotel.stats.api.configuration;

import com.gitlab.demo.hotel.stats.api.converters.StringToCountryConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.format.FormatterRegistry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@PropertySource("classpath:application.properties")
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
@EnableScheduling
public class APIConfiguration implements WebMvcConfigurer {


    @Value("${api.protocol}")
    private String apiProtocol;

    @Value("${api.host}")
    private String apiHost;

    @Value("${api.hotels.resource}")
    private String apiHotelsResource;

    public String getAPIProtocol() {
        return apiProtocol;
    }

    public String getAPIHost() {
        return apiHost;
    }

    public String getAPIHotelsResource() {
        return apiHotelsResource;
    }

    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.OAS_30)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToCountryConverter());
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
