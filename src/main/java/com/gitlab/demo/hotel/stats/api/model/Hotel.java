package com.gitlab.demo.hotel.stats.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Objects;

/**
 * This class represents a hotel. Each hotel has an unique id, a name, the country code where it belongs
 * and a score.
 */
@ApiModel(description = "Class representing a hotel from a country")
public class Hotel implements Comparable<Hotel> {

    @ApiModelProperty(notes = "Unique id for a hotel")
    @NotEmpty
    private String id;
    @ApiModelProperty(notes = "Name of the hotel")
    @NotEmpty
    private String name;
    @ApiModelProperty(notes = "Country where the hotel belongs")
    @NotEmpty
    @JsonProperty("country")
    private Country country;
    @ApiModelProperty(notes = "Puntuation of the hotel")
    @Min(0)
    @Max(10)
    private double score;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }


    @Override
    public int compareTo(Hotel o) {
        if (score > o.score) return 1;
        else if (score < o.score) return -1;
        else return 0;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", country=" + country +
                ", score=" + score +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hotel hotel = (Hotel) o;
        return score == hotel.score &&
                Objects.equals(id, hotel.id) &&
                Objects.equals(name, hotel.name) &&
                country == hotel.country;
    }
}
