package com.gitlab.demo.hotel.stats.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelStatsApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HotelStatsApiApplication.class, args);
    }

}
