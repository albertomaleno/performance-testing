# Performance Testing

Let's try to get a deeper knowledge of the concurrent Executors Service, while learning about performance testing, play with the
declarative paradigm of the streams, check the parallel streams and do some tests with Async programming
with the CompletableFuture API



## First part



### MicroBenchmarking



A microbenchmark tests small units of performance, like the time needed to call a method with a filtering implementation or another. Because of that, they're not used as much as mesobenchmarks and macrobenchmarks, because the application performance as a whole, is not measured.

Anyway, they can be useful in some situations and can be a good start point for benchmarking.

The microbenchmark found inside this application tests folder checks the overhead of using different filtering strategies. The default one,
filters the hotels first, by score, then alphabetically by name. After these comparisons, the duplications are removed from the list.

The second implementation only filters by score, removing the name overhead.

Finally, the third one, also filters by score, but then, it caches the output and does not do anything with the hotels
when remove duplicates method is called, removing another overhead.

Of course, the latest implementation is the fastest one, because it does not have to remove duplicates, which has a 
cost of O(n), being n, the number of elements present in the list. 

In order to create real world tests, a warmup period has been added to the test cases, so the jvm has time enough to
optimize the code. Another important thing added, is the use of volatile variables, because if the no read is done
from the variables, the jvm is able to skip the process.

**Finally, here are the computer specs used for the test**

| CPU                                       | Ram                |
| ----------------------------------------- | ------------------ |
| Ryzen 7 2700, 8/16 c/thr 3.2GHZ to 4.1GHZ | 16 GB CL16 3000MHZ |



**Total calculation time test**

| Number of hotels | Iterations  | Score range | Total time | Filtering strategy |
| ---------------- | ----------- | ----------- | ---------- | ------------------ |
|  1000            |    500      | 1.0 – 10.0  | 22 ms       | By score and name  |
|  1000            |    500      | 1.0 – 10.0  | 10 ms       | By score with duplications allowed          |
|  1000            |    500      | 1.0 – 10.0  | 19 ms       | By score with no duplication allowed  |



## Second part



### MesoBenchmarking



This way of benchmarking involves testing more than one module at the same time, in another way, involves testing closely related modules which form part of some kind of recurrent computation.

They can be measured in three ways:

- Batch test: Time to accomplish a certain task or elapsed time. It simply checks how long takes to do something, like retrieving 10000 records from a given time.
- Throughput: Amount of work that can be done in a certain period of time. Usually referred as transactions per second (TPS), request per second (RPS) or operations per second (OPS). This type of test is done after a great period of warm up.
- Response time: Amount of time between a client communicates with a server and a response is returned. Usually done with a "think time", which is a time in which the client waits until it sends another request. This is done to mimic user behavior on a system. They can be reported in percentile request, like saying the 90% of the request take 1 second or less to complete. Which can be also said as 1 second being the 90th% response time. It is useful in Java applications because there are pauses introduced by each Garbage Collector cycle.

Finally, an important think to keep in mind is variability or how results vary over time. Tests usually are created with random input to mimic user behavior, therefore, they do not show the same results. Checking if there is a regression or not can be solved by running the test multiple times and averaging results. Which, of course, will still show differences, but limiting them, we can hypothesize about the results and analyze if they are the same.



#### Quick overview of Data profiling



Is the process of reviewing data, understanding structure, relations and content. 

Using Intellij is possible to use the data profiler when running test to be able to measure cpu usage, memory allocation, method calls, among others. It will be helpful when analysing results in search of improving performance.

##### Considerations

A method call with a  higher CPU percentage usage doesn't mean its worst than another with less, it could simply mean that it has been called many more times. Taking into account that, it's possible to use the profiler to analyze data in order to improve application performance by focusing on methods that are the "hot" points of it so they get better optimized than others.






